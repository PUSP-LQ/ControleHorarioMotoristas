CREATE TABLE [dbo].[FolhaPonto](
	[UID] [uniqueidentifier] NOT NULL,
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[PessoaUID] [varchar](36) COLLATE Latin1_General_CI_AS NOT NULL,
	[Entrada] [datetime] NOT NULL,
	[Saida] [datetime] NOT NULL,
	[InicioIntervalo] [datetime] NULL,
	[FimIntervalo] [datetime] NULL,
	[Ocorrencia] [varchar](50) COLLATE Latin1_General_CI_AS NULL,
	[Compensacao] [int] NULL
) ON [PRIMARY]