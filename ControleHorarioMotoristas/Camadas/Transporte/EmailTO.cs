﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ControleHorarioMotoristas
{

    public class EmailTO
    {
        public int ID { get; set; }
        public string Remetente { get; set; }
        public string Destinatario { get; set; }
        public string Titulo { get; set; }
        public string Mensagem { get; set; }
        public DateTime Enviar { get; set; }
        public DateTime? Enviado { get; set; }
        public DateTime MOMCAD { get; set; }
        
    }
}
