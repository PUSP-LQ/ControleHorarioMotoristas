﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ControleHorarioMotoristas
{
    public class LogTO
    {
        public int ID { get; set; }
        public string NumeroUSP { get; set; }
        public string Descricao { get; set; }

        public string IPOrigem { get; set; }
        public string Navegador { get; set; }
        public string Tipo { get; set; }       
    }
}
