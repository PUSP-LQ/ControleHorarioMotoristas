﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ControleDeRonda.Camadas
{

    public class RondaRegistrosTO
    {
        public int ID { get; set; }
        public string UID { get; set; }
        
        //[CampoVirtual(true)]
        public DateTime MOMCAD { get; set; }

        //[CampoVirtual(true)]
        public DateTime MOMALT { get; set; }

        public string RondaTurnoUID { get; set; }
        public string RondaVeiculo { get; set; }
        public string RondaEquipe { get; set; }

        public string Local01 { get; set; }
        public string Local02 { get; set; }
        public string Local03 { get; set; }
        public string Local04 { get; set; }

        public string UsuarioLogadoUID { get; set; }
        public string UsuarioLogadoNome { get; set; }

        public RondaRegistrosTO()
        {
            this.UID = Guid.NewGuid().ToString();
            this.MOMCAD = DateTime.Now;
        }

        public RondaRegistrosTO(string uid)
        {
            RondaTurnoTO pessoa = new RondaTurnoTO();
            pessoa.UID = uid;
            //pessoa = RondaDAO.Get(pessoa);

            //this.ID = pessoa.ID;
            //this.UID = pessoa.UID;
            //this.Nome = pessoa.Nome;       
            //this.Usuario = pessoa.Usuario;
            //this.NumeroUSP = pessoa.NumeroUSP;
            //this.Apresentacao = pessoa.Apresentacao;


        }

    }
}
