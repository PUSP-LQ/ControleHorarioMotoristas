﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Configuration;

namespace ControleHorarioMotoristas
{
    public class LogDAO
    {
        public static int insert(string numeroUSP, string descricao, string IPOrigem, string navegador, string tipo)
        {
            string connectionString = ConfigurationManager.ConnectionStrings["ControledeMotoristasConnectionString"].ConnectionString;

            string sql = "INSERT INTO [Log] ([NumeroUSP], [Descricao], [IPOrigem], [Tipo], [Navegador]) VALUES (@NumeroUSP, @Descricao, @IPOrigem, @Tipo, @Navegador)";

            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();

                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = sql;
                cmd.Connection = conn;
                cmd.Parameters.Add(new SqlParameter("@NumeroUSP", numeroUSP));
                cmd.Parameters.Add(new SqlParameter("@Descricao", descricao));
                cmd.Parameters.Add(new SqlParameter("@IPOrigem", IPOrigem));
                cmd.Parameters.Add(new SqlParameter("@Tipo", tipo));
                cmd.Parameters.Add(new SqlParameter("@Navegador", navegador));

                return cmd.ExecuteNonQuery();

            }
        }
    }
}
