﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Configuration;

namespace ControleHorarioMotoristas
{
    public class EmailDAO
    {
        public static int Salvar(EmailTO email)
        {

            string connectionString = ConfigurationManager.ConnectionStrings["ControledeMotoristasConnectionString"].ConnectionString;

            string sql = "insert into Email (Remetente, Destinatario, Titulo, Mensagem, Enviar) ";
            sql += "values (@Remetente, @Destinatario, @Titulo, @Mensagem, @Enviar)";

            int result = 0;

            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();

                SqlCommand cmd = new SqlCommand();
                cmd = new SqlCommand();
                cmd.CommandText = sql;
                cmd.Connection = conn;
                cmd.Parameters.Add(new SqlParameter("@Remetente", email.Remetente));
                cmd.Parameters.Add(new SqlParameter("@Destinatario", email.Destinatario));
                cmd.Parameters.Add(new SqlParameter("@Titulo", email.Titulo));
                cmd.Parameters.Add(new SqlParameter("@Mensagem", email.Mensagem));
                cmd.Parameters.Add(new SqlParameter("@Enviar", email.Enviar));

                result = cmd.ExecuteNonQuery();

            }
            return result;
        }
    }
}
