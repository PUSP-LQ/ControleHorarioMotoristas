﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Configuration;

namespace ControleHorarioMotoristas
{
    public class UsuarioDAO
    {
        public static UsuarioTO get(string numeroUSP, string senha)
        {
            string connectionString = ConfigurationManager.ConnectionStrings["ControledeMotoristasConnectionString"].ConnectionString;

            string sql = "select * from Usuarios where numerousp = @numeroUSP and senha = @Senha";
            string anoAtual = string.Empty;
  
            UsuarioTO usuario = new UsuarioTO();

            using (SqlConnection conn = new SqlConnection(connectionString))
            {

                conn.Open();

                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = sql;
                cmd.Connection = conn;
                cmd.Parameters.Add(new SqlParameter("@numeroUSP", numeroUSP));
                cmd.Parameters.Add(new SqlParameter("@Senha", senha));

                SqlDataReader dr = cmd.ExecuteReader();

                if (dr.Read())
                {
                    usuario.Email = dr["Email"].ToString() ;
                    usuario.Nome = dr["Nome"].ToString();
                    usuario.NumeroUSP = dr["NumeroUSP"].ToString();
                    usuario.SecaoOuDepto = dr["SecaoOuDepto"].ToString();   
                    usuario.ID = (int) dr["ID"];   
                    usuario.Telefone = dr["Telefone"].ToString() ;   
                    usuario.Tipo = dr["Tipo"].ToString() ;   
                    usuario.UID = dr["UID"].ToString() ;   
                    usuario.Unidade = dr["Unidade"].ToString() ;   
                }
            }

            return usuario;
        }
    }
}
