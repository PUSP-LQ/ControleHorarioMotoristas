﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Data.SqlClient;

namespace ControleHorarioMotoristas
{
    public class FolhaPontoDAO //: GenericoDAO<RondaTurnoTO>
    {
        public static int saldoAcumuladoMinutos(string numeroUSP)
        {
            string sql = "select sum(SaldoMinutos) saldo from FolhaPonto where NumeroUSP = @NumeroUSP";

            using (SqlConnection conn = new SqlConnection(@"Data Source=066-006822\SQLEXPRESS;Initial Catalog=ControledeMotoristas;Integrated Security=True"))
            {
                conn.Open();

                SqlCommand cmd = new SqlCommand();
                cmd.Parameters.Add(new SqlParameter("@NumeroUSP", numeroUSP));
                cmd.CommandText = sql;
                cmd.Connection = conn;

                return (int)cmd.ExecuteScalar(); //.ExecuteReader();

            }
        }
    }
}
    
