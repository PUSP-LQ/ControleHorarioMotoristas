﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;

namespace ControleHorarioMotoristas.Formularios
{
    public partial class pageCadastroPonto : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                Calendar1.SelectedDate = DateTime.Today.AddDays(-1);
                LabelDiaSelecionado.Text = Calendar1.SelectedDate.ToShortDateString();
            }

            UsuarioTO usuario = Session["UsuarioLogado"] as UsuarioTO;
            Session["UnidadeUsuarioLogado"] = "PUSP-LQ";//usuario.Unidade;

        }

        protected void Calendar1_SelectionChanged(object sender, EventArgs e)
        {
            LabelDiaSelecionado.Text = Calendar1.SelectedDate.ToShortDateString();
            CustomValidatorCalendario.IsValid = (Calendar1.SelectedDate < DateTime.Today);

        }

        int realizouCompensacao30min = 30;

        protected void ButtonSave_Click(object sender, EventArgs e)
        {

            if (this.IsValid)
            {
                //@NumeroUSP, @Entrada, @Saida, @InicioIntervalo, @FimIntervalo, @Ocorrencia, @Compensacao               
                realizouCompensacao30min = CheckBox30Min.Checked ? 30 : 0;

                string DataHj = Calendar1.SelectedDate.ToShortDateString() + " ";

                string ultimoHorarioSaida = getUltimoHorarioSaida(DropDownListMotos.SelectedValue, DataHj + TextBoxHorarioEntrada.Text);

                bool descansou_11horas = Verificar_Descanso_de_11horas(ultimoHorarioSaida, DataHj + TextBoxHorarioEntrada.Text);

                string totalminutos = QtdMinutosTrabalhados();

                bool jornadaMaiorQue12Horas = Verificar_jornadaMaiorQue12Horas(totalminutos);

                bool intervaloMenorQue1Hora = Verificar_intervaloMenorQue1Hora(TextBoxHorarioInicioIntervalo.Text, TextBoxHorarioFimIntervalo.Text);

                bool ehFinalDeSemana = (Calendar1.SelectedDate.DayOfWeek == DayOfWeek.Saturday || Calendar1.SelectedDate.DayOfWeek == DayOfWeek.Sunday);
                
                string tipoHora =  ehFinalDeSemana ? "02 - Final de Semana" : "01 - Normal";

                string ocorrencia = HouveOcorrencia(totalminutos) + (descansou_11horas ? string.Empty : " | Não descansou 11 horas");
                
                if (ehFinalDeSemana)
                {
                    ocorrencia += " | Final de semana";
                }

                if (jornadaMaiorQue12Horas)
                {
                    ocorrencia += " | Jornada maior que 12 horas";
                }

                if (intervaloMenorQue1Hora)
                {
                    ocorrencia += " | Intervalo menor que 1 hora";
                }

                SqlDataSourceInsert.InsertParameters.Add("NumeroUSP", DropDownListMotos.SelectedValue);

                SqlDataSourceInsert.InsertParameters.Add("Entrada", System.Data.DbType.DateTime, DataHj + TextBoxHorarioEntrada.Text);

                SqlDataSourceInsert.InsertParameters.Add("Saida", System.Data.DbType.DateTime, DataHj + TextBoxHorarioSaida.Text);

                SqlDataSourceInsert.InsertParameters.Add("InicioIntervalo", System.Data.DbType.DateTime, DataHj + TextBoxHorarioInicioIntervalo.Text);

                SqlDataSourceInsert.InsertParameters.Add("FimIntervalo", System.Data.DbType.DateTime, DataHj + TextBoxHorarioFimIntervalo.Text);

                SqlDataSourceInsert.InsertParameters.Add("Ocorrencia", ocorrencia);
                SqlDataSourceInsert.InsertParameters.Add("Compensacao", realizouCompensacao30min.ToString() + " Minutos");
                SqlDataSourceInsert.InsertParameters.Add("QtdMinutosTrabalhados", totalminutos);
                SqlDataSourceInsert.InsertParameters.Add("TipoHora", tipoHora);
                SqlDataSourceInsert.InsertParameters.Add("SaldoMinutos", QtdSaldoMinutos(totalminutos, ehFinalDeSemana));


                SqlDataSourceInsert.Insert();


            }
        }

        private bool Verificar_intervaloMenorQue1Hora(string InicioIntervalo, string FimIntervalo)
        {
            if (!(string.IsNullOrEmpty(InicioIntervalo) && string.IsNullOrEmpty(FimIntervalo)))
            {
                int totalminutosIntervalo = Convert.ToInt32(Convert.ToDateTime(FimIntervalo).Subtract(Convert.ToDateTime(InicioIntervalo)).TotalMinutes);
                return totalminutosIntervalo < 60;
            }
            return false;
        }

        private bool Verificar_jornadaMaiorQue12Horas(string totalminutos)
        {
            return Convert.ToInt32(totalminutos) > 12 * 60;
        }

        private bool Verificar_Descanso_de_11horas(string ultimoHorario, string HorariodeEntrada)
        {
            int totalminutos = Convert.ToInt32(Convert.ToDateTime(HorariodeEntrada).Subtract(Convert.ToDateTime(ultimoHorario)).TotalMinutes);

            return totalminutos > 11 * 60;
        }

        private string QtdSaldoMinutos(string totalminutos, bool ehFinalDeSemana)
        {
            return ehFinalDeSemana ? totalminutos : (Convert.ToInt32(totalminutos) - (8 * 60 + realizouCompensacao30min)).ToString();
        }

        private string QtdMinutosTrabalhados()
        {
            int totalminutos = 0;

            if (string.IsNullOrEmpty(TextBoxHorarioInicioIntervalo.Text))
            {
                totalminutos = Convert.ToInt32(Convert.ToDateTime(TextBoxHorarioSaida.Text).Subtract(Convert.ToDateTime(TextBoxHorarioEntrada.Text)).TotalMinutes);

            }
            else
            {
                totalminutos = Convert.ToInt32(Convert.ToDateTime(TextBoxHorarioInicioIntervalo.Text).Subtract(Convert.ToDateTime(TextBoxHorarioEntrada.Text)).TotalMinutes);
                totalminutos += Convert.ToInt32(Convert.ToDateTime(TextBoxHorarioSaida.Text).Subtract(Convert.ToDateTime(TextBoxHorarioFimIntervalo.Text)).TotalMinutes);
            }


            return totalminutos.ToString();
        }

        private string HouveOcorrencia(string totalminutos)
        {

            return 8 * 60 + realizouCompensacao30min == Convert.ToInt32(totalminutos) ? string.Empty : (Convert.ToInt32(totalminutos) > 8 * 60 + realizouCompensacao30min ? "Hora extra" : "Não completou a jornada");

        }


        private string getUltimoHorarioSaida(string numeroUSP, string MomentoEntrada)
        {
            string sql = "select top 1 saida from FolhaPonto where NumeroUSP = @NumeroUSP and saida < @entrada order by id desc";

            using (SqlConnection conn = new SqlConnection(@"Data Source=066-006822\SQLEXPRESS;Initial Catalog=ControledeMotoristas;Integrated Security=True"))
            {               
                conn.Open();

                SqlCommand cmd = new SqlCommand();
                cmd.Parameters.Add(new SqlParameter("@NumeroUSP", numeroUSP));
                cmd.Parameters.Add(new SqlParameter("@entrada", Convert.ToDateTime(MomentoEntrada)));
                cmd.CommandText = sql;
                cmd.Connection = conn;

                return Convert.ToDateTime(cmd.ExecuteScalar()).ToString(); //.ExecuteReader();

            }
        }

        protected void GridView2_SelectedIndexChanged(object sender, EventArgs e)
        {
            //
        }


     }
}
    



