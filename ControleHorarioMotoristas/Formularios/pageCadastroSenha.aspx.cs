﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ControleHorarioMotoristas.Formularios
{
    public partial class pageCadastroSenha : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void ButtonSenha_Click(object sender, EventArgs e)
        {
            if (this.Page.IsValid)
            {
                UsuarioTO usuario = Session["UsuarioLogado"] as UsuarioTO;

                SqlDataSourceAlterSenha.UpdateParameters["Senha"] = new Parameter(UtilBO.GetMD5Hash(TextBoxSenha.Text));
                SqlDataSourceAlterSenha.UpdateParameters["NumeroUSP"] = new Parameter(usuario.NumeroUSP);


                SqlDataSourceAlterSenha.Update();

                TextBoxConfSenha.Text = string.Empty;
                TextBoxSenha.Text = string.Empty;


                LabelMensagem.Text = "Senha salva com sucesso.";
            }
        }
    }
}
