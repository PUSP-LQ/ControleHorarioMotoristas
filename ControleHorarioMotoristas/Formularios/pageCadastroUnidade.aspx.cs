﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ControleHorarioMotoristas.Formularios
{
    public partial class pageCadastroUnidade : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack)
            {
                LinkButtonNovo.Text = "Incluir novo usuário";
                LinkButtonNovo.Enabled = true;
            }
        }

        protected void LinkButtonNovo_Click(object sender, EventArgs e)
        {
            PanelDetails.Visible = true;
            LinkButtonNovo.Text = "Incluindo novo usuário";
            LinkButtonNovo.Enabled = false;
        }

        protected void GridViewUsuarios_SelectedIndexChanged(object sender, EventArgs e)
        {
            PanelDetails.Visible = true;

            TextBoxNumero.Text = GridViewUnidadeUSP.SelectedRow.Cells[1].Text;
            TextBoxCodigo.Text = GridViewUnidadeUSP.SelectedRow.Cells[2].Text;
            TextBoxDescricao.Text = GridViewUnidadeUSP.SelectedRow.Cells[3].Text;

            TextBoxNumero.Enabled = false;
            TextBoxCodigo.Enabled = false;
        }

        protected void ButtonSaveInsert_Click(object sender, EventArgs e)
        {
            SqlDataSourceSave.InsertParameters["Numero"] = new Parameter(TextBoxNumero.Text);            
            SqlDataSourceSave.InsertParameters["Descricao"] = new Parameter(TextBoxDescricao.Text);
            SqlDataSourceSave.InsertParameters["Codigo"] = new Parameter(TextBoxCodigo.Text);

            SqlDataSourceSave.Insert();

            clearControls();

        }

        protected void ButtonSaveUpdate_Click(object sender, EventArgs e)
        {
            SqlDataSourceSave.UpdateParameters["Descricao"] = new Parameter(TextBoxDescricao.Text);
            SqlDataSourceSave.UpdateParameters["Codigo"] = new Parameter(TextBoxCodigo.Text);

            clearControls();

            SqlDataSourceSave.Update();

        }

        private void clearControls()
        {
            TextBoxNumero.Text = string.Empty;
            TextBoxDescricao.Text = string.Empty;
            TextBoxCodigo.Text = string.Empty;

        }



    }
}
