﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MasterOrbita.Master" AutoEventWireup="true" CodeBehind="pageCadastroSenha.aspx.cs" Inherits="ControleHorarioMotoristas.Formularios.pageCadastroSenha" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
        <br />

<asp:SqlDataSource ID="SqlDataSourceAlterSenha" runat="server" 
        ConnectionString="<%$ ConnectionStrings:ControledeMotoristasConnectionString %>" 
        UpdateCommand="UPDATE [Usuarios] SET [Senha] = @Senha WHERE [NumeroUSP] = @NumeroUSP">        
</asp:SqlDataSource>

<br />
<br />
        <table style="width:100%;">
            <tr>
                <td>
                    Senha:</td>
                <td>
                    <asp:TextBox ID="TextBoxSenha" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidatorSenha" runat="server" 
                        ErrorMessage="Digitar senha" Display="Dynamic" ControlToValidate="TextBoxSenha"></asp:RequiredFieldValidator>
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    Confirmar senha:</td>
                <td>
                    <asp:TextBox ID="TextBoxConfSenha" runat="server"></asp:TextBox>
                    <asp:CompareValidator ID="CompareValidatorConfSenha" runat="server" ControlToCompare="TextBoxConfSenha"  ControlToValidate="TextBoxSenha"
                        ErrorMessage="Senha não confere." Display="Dynamic"></asp:CompareValidator>
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td>
                    <asp:Button ID="ButtonSenha" runat="server" onclick="ButtonSenha_Click" Text="Salvar Senha" />
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="LabelMensagem" runat="server" Text=""></asp:Label>
                </td>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
        </table>
        <br />



</asp:Content>
