﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MasterOrbita.Master" AutoEventWireup="true" CodeBehind="pageCadastroMotorista.aspx.cs" Inherits="ControleHorarioMotoristas.Formularios.pageCadastroMotorista" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<br />

    Selecione a Unidade: 
    <asp:DropDownList ID="DropDownListUnidade" runat="server" AutoPostBack="true">
        <asp:ListItem>PUSP-LQ</asp:ListItem>
        <asp:ListItem>PUSP-RP</asp:ListItem>
        </asp:DropDownList>
    
    <asp:Label ID="LabelMsg" runat="server" Text=""></asp:Label>       
<br /><br />

<div style ="width:750px; overflow:auto;">
    <asp:GridView ID="GridViewUsuarios" runat="server" AllowSorting="True" 
        AutoGenerateColumns="False" CellPadding="4" DataKeyNames="NumeroUSP" 
        DataSourceID="SqlDataSource1" 
        EmptyDataText="Não há dados a serem exibidos." ForeColor="#333333" 
        GridLines="None" EnableViewState="False" 
        onselectedindexchanged="GridViewUsuarios_SelectedIndexChanged">
        <RowStyle BackColor="#EFF3FB" />
        <Columns>
            <asp:CommandField ShowSelectButton="True" SelectText="Editar" />
            <asp:BoundField DataField="NumeroUSP" HeaderText="Número USP" ReadOnly="True" 
                SortExpression="NumeroUSP" />
            <asp:BoundField DataField="Nome" HeaderText="Nome" SortExpression="Nome" />
            <asp:BoundField DataField="Email" HeaderText="E-mail" SortExpression="Email" />
            <asp:BoundField DataField="Telefone" HeaderText="Telefone" 
                SortExpression="Telefone" />             
        </Columns>
        <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
        <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
        <EditRowStyle BackColor="#2461BF" />
        <AlternatingRowStyle BackColor="White" />
    </asp:GridView>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
        ConnectionString="<%$ ConnectionStrings:ControledeMotoristasConnectionString %>" 
        ProviderName="<%$ ConnectionStrings:ControledeMotoristasConnectionString.ProviderName %>" 
        SelectCommand="SELECT mot.[ID], mot.[UID], [NumeroUSP], [Nome], [Email], [Telefone] FROM [Usuarios] mot where Unidade = @unidade order by nome">
        <SelectParameters><asp:ControlParameter ControlID="DropDownListUnidade" Name="Unidade" Type="String" /></SelectParameters>
    </asp:SqlDataSource>
</div>

    
    <br />
        <asp:LinkButton ID="LinkButtonNovo" runat="server" 
        onclick="LinkButtonNovo_Click">Incluir novo usuário</asp:LinkButton>
    <br />
    <br />
    
    <asp:Panel ID="PanelDetails" runat="server" GroupingText="Detalhes">
        <br />
        <table style="width:100%;">
            <tr>
                <td>
                    Nome:</td>
                <td>
                    <asp:TextBox ID="TextBoxNome" runat="server" MaxLength="160"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="TextBoxNome"
                        Display="Dynamic" ErrorMessage="(*) Campo requirido"></asp:RequiredFieldValidator>
                </td>
                <td>
                    E-mail:</td>
                <td>
                    <asp:TextBox ID="TextBoxEmail" runat="server" MaxLength="160"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="TextBoxEmail"
                        Display="Dynamic" ErrorMessage="(*) Campo requirido"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    Telefone:</td>
                <td>
                    <asp:TextBox ID="TextBoxTelefone" runat="server" MaxLength="160"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="TextBoxTelefone"
                        Display="Dynamic" ErrorMessage="(*) Campo requirido"></asp:RequiredFieldValidator>
                </td>
                <td>
                    Numero USP:</td>
                <td>
                    <asp:TextBox ID="TextBoxNumeroUSP" runat="server" MaxLength="160"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="TextBoxNumeroUSP"
                        Display="Dynamic" ErrorMessage="(*) Campo requirido"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    Senha:</td>
                <td>
                    <asp:LinkButton ID="LinkButtonNovaSenha" runat="server" 
                        onclick="LinkButtonNovaSenha_Click">Enviar uma nova senha</asp:LinkButton>
                    <asp:TextBox ID="TextBoxSenha" runat="server" MaxLength="160" TextMode="Password"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="TextBoxSenha"
                        Display="Dynamic" ErrorMessage="(*) Campo requirido"></asp:RequiredFieldValidator>
                </td>
                <td>
                    Tipo:</td>
                <td>
                    <asp:DropDownList ID="DropDownListTipo" runat="server">
                        <asp:ListItem Selected="True"></asp:ListItem>
                        <asp:ListItem>Administrativo</asp:ListItem>
                        <asp:ListItem>Motorista</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
                <td>
                    Seção:</td>
                <td>
                    <asp:DropDownList ID="DropDownListSecao" runat="server">
                        <asp:ListItem>SCINFOR</asp:ListItem>
                        <asp:ListItem Selected="True">SCTRANS</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
                <td>
                    <asp:Button ID="ButtonSaveInsert" runat="server" Text="Salvar" 
                        onclick="ButtonSaveInsert_Click" />
                    <asp:Button ID="ButtonSaveUpdate" runat="server" Text="Salvar alterações" 
                        onclick="ButtonSaveUpdate_Click" />
                    <asp:Button ID="ButtonCancelar" runat="server" Text="Cancelar" CausesValidation="false"
                        onclick="ButtonCancelar_Click" />

                </td>
            </tr>
        </table>
        <br />
    </asp:Panel>
    
<asp:SqlDataSource ID="SqlDataSourceSave" runat="server" 
        ConnectionString="<%$ ConnectionStrings:ControledeMotoristasConnectionString %>" 
        InsertCommand="INSERT INTO [Usuarios] ([NumeroUSP], [Nome], [Email], [Telefone], [Unidade], [Secao], [Tipo], [Senha]) VALUES (@NumeroUSP, @Nome, @Email, @Telefone, @Unidade, @Secao, @Tipo, @Senha)" 
        ProviderName="<%$ ConnectionStrings:ControledeMotoristasConnectionString.ProviderName %>" 
        
        UpdateCommand="UPDATE [Usuarios] SET [Nome] = @Nome, [Email] = @Email, [Telefone] = @Telefone, [Secao] = @Secao, [Tipo] = @Tipo WHERE [NumeroUSP] = @NumeroUSP" 
        oninserting="SqlDataSourceSave_Inserting">
        <InsertParameters>

            <asp:ControlParameter  ControlID="TextBoxNome"  Name="Nome" Type="String" />
            <asp:ControlParameter  ControlID="TextBoxEmail"  Name="Email" Type="String" />           
            <asp:ControlParameter  ControlID="TextBoxTelefone"  Name="Telefone" Type="String" />
            <asp:ControlParameter  ControlID="TextBoxNumeroUSP"  Name="NumeroUSP" Type="String" />
            <asp:ControlParameter  ControlID="TextBoxSenha"  Name="Senha" Type="String" />
            
            <asp:ControlParameter  ControlID="DropDownListTipo"  Name="Tipo" Type="String" />
            <asp:ControlParameter  ControlID="DropDownListSecao"  Name="Secao" Type="String" />
            <asp:ControlParameter  ControlID="DropDownListUnidade"  Name="Unidade" Type="String" />
            
                        
        </InsertParameters>
        <UpdateParameters>
        
            <asp:ControlParameter  ControlID="TextBoxNome"  Name="Nome" Type="String" />
            <asp:ControlParameter  ControlID="TextBoxEmail"  Name="Email" Type="String" />           
            <asp:ControlParameter  ControlID="TextBoxTelefone"  Name="Telefone" Type="String" />
            <asp:ControlParameter  ControlID="TextBoxNumeroUSP"  Name="NumeroUSP" Type="String" />
            
            <asp:ControlParameter  ControlID="DropDownListTipo"  Name="Tipo" Type="String" />
            <asp:ControlParameter  ControlID="DropDownListSecao"  Name="Secao" Type="String" />

            
        </UpdateParameters>        
    </asp:SqlDataSource>     
        
      
</asp:Content>



