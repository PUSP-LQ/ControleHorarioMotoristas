﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ControleHorarioMotoristas.Formularios
{
    public partial class pageCadastroMotorista : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //if (IsPostBack)
            //{
            //    LinkButtonNovo.Text = "Incluir novo usuário";
            //    LinkButtonNovo.Enabled = true;
            //}

            PanelDetails.Visible = false;
        }

        protected void LinkButtonNovo_Click(object sender, EventArgs e)
        {
            PanelDetails.Visible = true;
            LinkButtonNovo.Text = "Incluindo novo usuário";
            LinkButtonNovo.Enabled = false;
            LinkButtonNovaSenha.Visible = false;
            ButtonSaveUpdate.Visible = false;
            ButtonSaveInsert.Visible = true;

        }

        protected void GridViewUsuarios_SelectedIndexChanged(object sender, EventArgs e)
        {
            TextBoxNome.Text = GridViewUsuarios.SelectedRow.Cells[2].Text;
            TextBoxEmail.Text = GridViewUsuarios.SelectedRow.Cells[3].Text;
            TextBoxTelefone.Text = GridViewUsuarios.SelectedRow.Cells[4].Text;
            TextBoxNumeroUSP.Text = GridViewUsuarios.SelectedRow.Cells[1].Text;
            TextBoxSenha.Text = GridViewUsuarios.SelectedRow.Cells[0].Text;

            DropDownListTipo.SelectedIndex = 0;
            DropDownListSecao.SelectedIndex = 0;

                PanelDetails.Visible = true;
                LinkButtonNovo.Text = "Editando usuário";
                LinkButtonNovo.Enabled = false;

                ButtonSaveUpdate.Visible = true;
                ButtonSaveInsert.Visible = false;

                LinkButtonNovaSenha.Visible = true;
                TextBoxSenha.Visible = false;
        }

        protected void ButtonSaveInsert_Click(object sender, EventArgs e)
        {
            SqlDataSourceSave.Insert();

            clearControls();

        }

        protected void ButtonCancelar_Click(object sender, EventArgs e)
        {
            PanelDetails.Visible = false;
            LinkButtonNovo.Text = "Incluir novo usuário";
            LinkButtonNovo.Enabled = true;
        }


        protected void ButtonSaveUpdate_Click(object sender, EventArgs e)
        {
            SqlDataSourceSave.UpdateParameters["@Nome"] = new Parameter(TextBoxNome.Text);
            SqlDataSourceSave.UpdateParameters["@Email"] = new Parameter(TextBoxEmail.Text);

            SqlDataSourceSave.UpdateParameters["@Telefone"] = new Parameter(TextBoxTelefone.Text);
            SqlDataSourceSave.UpdateParameters["@NumeroUSP"] = new Parameter(TextBoxNumeroUSP.Text);
            //SqlDataSourceSave.UpdateParameters["Senha"] = new Parameter(TextBoxSenha.Text);

            SqlDataSourceSave.UpdateParameters["@Tipo"] = new Parameter(DropDownListTipo.SelectedValue);
            SqlDataSourceSave.UpdateParameters["@Secao"] = new Parameter(DropDownListSecao.SelectedValue);

            SqlDataSourceSave.Update();

            clearControls();

        }

        private void clearControls()
        {
            foreach (var controle in this.Page.Form.Controls)
            {
                if (typeof(TextBox) == controle.GetType())
                    ((TextBox)controle).Text = string.Empty;
            }
        }

        protected void SqlDataSourceSave_Inserting(object sender, SqlDataSourceCommandEventArgs e)
        {
            e.Command.Parameters["@Senha"].Value = UtilBO.GetMD5Hash(TextBoxSenha.Text);
        }

        protected void LinkButtonNovaSenha_Click(object sender, EventArgs e)
        {
            // envia uma nova senha pro e-mail.
            EmailTO email = new EmailTO();
            email.Destinatario = GridViewUsuarios.SelectedRow.Cells[3].Text;
            email.Remetente = "informatica.lq@usp.br";
            email.Enviar = DateTime.Now;
            email.Titulo = "Recuperação de senha / Nova senha";
            email.Mensagem = "Seu nova senha é xxxx";

            EmailDAO.Salvar(email);

            LabelMsg.Text = "Nova senha enviado com sucesso para " + email.Destinatario;

            PanelDetails.Visible = false;
            LinkButtonNovo.Text = "Incluir novo usuário";
            LinkButtonNovo.Enabled = true;

        }
    }
}
