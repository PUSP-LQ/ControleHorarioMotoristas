﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MasterOrbita.Master" AutoEventWireup="true" CodeBehind="pageCadastroFeriado.aspx.cs" Inherits="ControleHorarioMotoristas.Formularios.pageCadastroFeriado" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <p>
        <asp:GridView ID="GridView1" runat="server" AllowPaging="True" 
            AllowSorting="True" AutoGenerateColumns="False" CellPadding="4" DataSourceID="SqlDataSource1" 
            EmptyDataText="There are no data records to display." ForeColor="#333333" 
            GridLines="None">
            <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
            <Columns>
                <asp:BoundField DataField="Nr_Ano" HeaderText="Nr_Ano" 
                    SortExpression="Nr_Ano" />
                <asp:BoundField DataField="Nr_Mes" HeaderText="Nr_Mes" 
                    SortExpression="Nr_Mes" />
                <asp:BoundField DataField="Nr_Dia" HeaderText="Nr_Dia" 
                    SortExpression="Nr_Dia" />
                <asp:BoundField DataField="Tp_Feriado" HeaderText="Tp_Feriado" 
                    SortExpression="Tp_Feriado" />
                <asp:BoundField DataField="Ds_Feriado" HeaderText="Ds_Feriado" 
                    SortExpression="Ds_Feriado" />
                <asp:BoundField DataField="Sg_UF" HeaderText="Sg_UF" 
                    SortExpression="Sg_UF" />
            </Columns>
            <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
            <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
            <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
            <AlternatingRowStyle BackColor="White" />
        </asp:GridView>
    </p>
    <p>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
            ConnectionString="<%$ ConnectionStrings:ControledeMotoristasConnectionString %>" 
            ProviderName="<%$ ConnectionStrings:ControledeMotoristasConnectionString.ProviderName %>" 
            SelectCommand="SELECT * FROM [Feriado]"
            InsertCommand="SELECT * FROM [Feriado]"></asp:SqlDataSource>
    </p>
    <p>
        <asp:DetailsView ID="DetailsView1" runat="server" AllowPaging="True" 
            AutoGenerateRows="False" CellPadding="4" DataSourceID="SqlDataSource1" 
            ForeColor="#333333" GridLines="None" Height="50px" Width="125px">
            <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
            <CommandRowStyle BackColor="#FFFFC0" Font-Bold="True" />
            <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
            <FieldHeaderStyle BackColor="#FFFF99" Font-Bold="True" />
            <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
            <Fields>
                <asp:BoundField DataField="Nr_Ano" HeaderText="Nr_Ano" 
                    SortExpression="Nr_Ano" />
                <asp:BoundField DataField="Nr_Mes" HeaderText="Nr_Mes" 
                    SortExpression="Nr_Mes" />
                <asp:BoundField DataField="Nr_Dia" HeaderText="Nr_Dia" 
                    SortExpression="Nr_Dia" />
                <asp:BoundField DataField="Tp_Feriado" HeaderText="Tp_Feriado" 
                    SortExpression="Tp_Feriado" />
                <asp:BoundField DataField="Ds_Feriado" HeaderText="Ds_Feriado" 
                    SortExpression="Ds_Feriado" />
                <asp:BoundField DataField="Sg_UF" HeaderText="Sg_UF" 
                    SortExpression="Sg_UF" />
            </Fields>
            <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
            <AlternatingRowStyle BackColor="White" />
        </asp:DetailsView>
        <br />
    </p>
    <p>
    </p>
    <p>
    </p>
    <p>
    </p>
    <p>
    </p>
</asp:Content>
