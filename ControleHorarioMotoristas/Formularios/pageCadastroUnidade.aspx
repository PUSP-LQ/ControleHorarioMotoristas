﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MasterOrbita.Master" AutoEventWireup="true" CodeBehind="pageCadastroUnidade.aspx.cs" Inherits="ControleHorarioMotoristas.Formularios.pageCadastroUnidade" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<br />

<div style ="width:750px; overflow:auto;">
    <asp:GridView ID="GridViewUnidadeUSP" runat="server" AllowSorting="True" 
        AutoGenerateColumns="False" CellPadding="4" DataKeyNames="NumeroUSP" 
        DataSourceID="SqlDataSource1" 
        EmptyDataText="Sem dados para exibição." ForeColor="#333333" 
        GridLines="None" EnableViewState="False">
        <RowStyle BackColor="#EFF3FB" />
        <Columns>
            <asp:HyperLinkField Text="Editar" />
            <asp:BoundField DataField="Numero" HeaderText="Número" ReadOnly="True" 
                SortExpression="Numero" />
            <asp:BoundField DataField="Codigo" HeaderText="Código" SortExpression="Codigo" />
            <asp:BoundField DataField="Descricao" HeaderText="Descrição" SortExpression="Descrição" />
        </Columns>
        <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
        <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
        <EditRowStyle BackColor="#2461BF" />
        <AlternatingRowStyle BackColor="White" />
    </asp:GridView>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
        ConnectionString="<%$ ConnectionStrings:ControledeMotoristasConnectionString %>" 
        ProviderName="<%$ ConnectionStrings:ControledeMotoristasConnectionString.ProviderName %>" 
        SelectCommand="SELECT [ID], [UID], [NumeroUSP], [Codigo], [Descricao] FROM [UnidadeUSP] order by Descricao">
    </asp:SqlDataSource>
</div>

    
    <br />
        <asp:LinkButton ID="LinkButtonNovo" runat="server" 
        onclick="LinkButtonNovo_Click">Incluir nova unidade</asp:LinkButton>
    <br />
    <br />
    
    <asp:Panel ID="PanelDetails" runat="server" GroupingText="Detalhes" Visible="false">
        <br />
        <table style="width:90%;">
            <tr>
                <td>
                    Número:</td>
                <td>
                    <asp:TextBox ID="TextBoxNumero" runat="server" MaxLength="160"></asp:TextBox>
                </td>
                <td>
                    Código:</td>
                <td>
                    <asp:TextBox ID="TextBoxCodigo" runat="server" MaxLength="160"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                        Display="Dynamic" ErrorMessage="(*) Campo requirido"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    Descrição:</td>
                <td>
                    <asp:TextBox ID="TextBoxDescricao" runat="server" MaxLength="160"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" 
                        Display="Dynamic" ErrorMessage="(*) Campo requirido"></asp:RequiredFieldValidator>
                </td>
                <td>
                    </td>
                <td>
                    
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
                <td>
                    <asp:Button ID="ButtonSaveInsert" runat="server" Text="Salvar" 
                        onclick="ButtonSaveInsert_Click" />
                         <asp:Button ID="ButtonSaveUpdate" runat="server" Text="Salvar alterações" 
                        onclick="ButtonSaveUpdate_Click" />
                </td>
            </tr>
        </table>
        <br />
    </asp:Panel>
    
<asp:SqlDataSource ID="SqlDataSourceSave" runat="server" 
        ConnectionString="<%$ ConnectionStrings:ControledeMotoristasConnectionString %>" 
        InsertCommand="INSERT INTO [UnidadeUSP] ([Numero], [Codigo], [Descricao]) VALUES (@Numero, @Codigo, @Descricao)" 
        ProviderName="<%$ ConnectionStrings:ControledeMotoristasConnectionString.ProviderName %>" 
        UpdateCommand="UPDATE [UnidadeUSP] SET [Descricao] = @Descricao WHERE [Codigo] = @Codigo">
        <InsertParameters>
            <asp:Parameter Name="Numero" Type="String" />
            <asp:Parameter Name="Codigo" Type="String" />
            <asp:Parameter Name="Descricao" Type="String" />           
        </InsertParameters>
        <UpdateParameters>
            <asp:Parameter Name="Descricao" Type="String" />
            <asp:Parameter Name="Codigo" Type="String" />
        </UpdateParameters>
    </asp:SqlDataSource>     
        
      
</asp:Content>


