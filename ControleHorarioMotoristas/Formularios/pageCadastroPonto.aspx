<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MasterOrbita.Master" AutoEventWireup="true" CodeBehind="pageCadastroPonto.aspx.cs" Inherits="ControleHorarioMotoristas.Formularios.pageCadastroPonto" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

 <script src="http://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.5.js" type="text/javascript"></script>

    <script src="http://cloud.github.com/downloads/digitalBush/jquery.maskedinput/jquery.maskedinput-1.3.min.js"
        type="text/javascript"></script>

    <script type="text/javascript">
        $(document).ready(function() {
            $("input[type=text][id*=TextBoxHorario]").mask("99:99");
        });
  
    </script>

    Motorista:

<asp:DropDownList ID="DropDownListMotos" runat="server" 
        DataSourceID="SqlDataSourceMotos" DataTextField="Nome" 
        DataValueField="NumeroUSP" AutoPostBack="True">
</asp:DropDownList>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Por favor selecione um motorista" ControlToValidate="DropDownListMotos"></asp:RequiredFieldValidator>
    <br />
    <br />
    <asp:GridView ID="GridView2" runat="server" EnableViewState="false"
        DataSourceID="SqlDataSourceMotoDetails" AutoGenerateColumns="false" 
        onselectedindexchanged="GridView2_SelectedIndexChanged">
        <Columns>       
            <asp:BoundField DataField="NumeroUSP" HeaderText="N�mero USP" ItemStyle-HorizontalAlign="Center" />        
            <asp:BoundField DataField="Telefone" HeaderText="Telefone" ItemStyle-HorizontalAlign="Center" />  
            <asp:BoundField DataField="Email" HeaderText="E-mail" ItemStyle-HorizontalAlign="Center" />  
            <asp:TemplateField ItemStyle-HorizontalAlign="Right">
                <HeaderTemplate>Saldo acumulado</HeaderTemplate>
                <ItemTemplate> 
                        <%# Convert.ToInt32(Eval("SaldoMinutos")) / 60 %> hr <%# Convert.ToInt32(Eval("SaldoMinutos")) % 60%> Minutos
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
    <br />

    <table style="width:90%;">
        <tr>
            <td rowspan="7">
                    <asp:Calendar ID="Calendar1" runat="server" Height="180px" Width="200px" 
                        onselectionchanged="Calendar1_SelectionChanged" BackColor="White" 
                        BorderColor="#999999" CellPadding="4" DayNameFormat="Shortest" 
                        Font-Names="Verdana" Font-Size="8pt" ForeColor="Black">
                        <SelectedDayStyle BackColor="#666666" Font-Bold="True" ForeColor="White" />
                        <SelectorStyle BackColor="#CCCCCC" />
                        <WeekendDayStyle BackColor="#FFFFCC" />
                        <TodayDayStyle BackColor="#CCCCCC" ForeColor="Black" />
                        <OtherMonthDayStyle ForeColor="#808080" />
                        <NextPrevStyle VerticalAlign="Bottom" />
                        <DayHeaderStyle BackColor="#CCCCCC" Font-Bold="True" Font-Size="7pt" />
                        <TitleStyle BackColor="#999999" BorderColor="Black" Font-Bold="True" />
                    </asp:Calendar>
            <asp:CustomValidator ID="CustomValidatorCalendario" runat="server" ErrorMessage="Selecione uma data menor do que hoje"></asp:CustomValidator>
            </td>
            <td>
                </td>
            <td>
                </td>
        </tr>
        <tr>

            <td Width="120">
                Data selecionada: </td>
            <td>
                <asp:Label ID="LabelDiaSelecionado" runat="server" Text=""></asp:Label></td>
            <td>
                
            </td>                
        </tr>                      
        <tr>
            <td>
                Hor�rio Entrada</td>
            <td>
                <asp:TextBox ID="TextBoxHorarioEntrada" runat="server"></asp:TextBox></td>
            <td>
                 <asp:RegularExpressionValidator ID="rev" runat="server" ErrorMessage="Hor�rio Inv�lido" 
                                                 ControlToValidate="TextBoxHorarioEntrada" ValidationExpression="(([01][0-9])|(2[0-3])):([0-5][0-9])$">
                                                 <!-- ^([0-1]?[0-9]|2[0-4]):([0-5][0-9])(:[0-5][0-9])?$ -->
                </asp:RegularExpressionValidator>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Por favor digite um hor�rio." ControlToValidate="TextBoxHorarioEntrada"></asp:RequiredFieldValidator>
    </td>
        </tr>
        <tr>
            <td>
                In�cio intervalo</td>
            <td><asp:TextBox ID="TextBoxHorarioInicioIntervalo" runat="server"></asp:TextBox></td>
            <td>
                                 <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="Hor�rio Inv�lido" 
                                                 ControlToValidate="TextBoxHorarioInicioIntervalo" ValidationExpression="(([01][0-9])|(2[0-3])):([0-5][0-9])$"></asp:RegularExpressionValidator>
</td>
        </tr>
        <tr>
            <td>
                Fim intervalo</td>
            <td>
                <asp:TextBox ID="TextBoxHorarioFimIntervalo" runat="server"></asp:TextBox></td>
            <td>
                                 <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ErrorMessage="Hor�rio Inv�lido" Display="Dynamic"
                                                 ControlToValidate="TextBoxHorarioFimIntervalo" ValidationExpression="(([01][0-9])|(2[0-3])):([0-5][0-9])$"></asp:RegularExpressionValidator>
                                 
</td>
        </tr>
        <tr>
            <td>
                Hor�rio Sa�da</td>
            <td>
                <asp:TextBox ID="TextBoxHorarioSaida" runat="server"></asp:TextBox></td>
            <td>
                                 <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ErrorMessage="Hor�rio Inv�lido" Display="Dynamic"
                                                 ControlToValidate="TextBoxHorarioSaida" ValidationExpression="(([01][0-9])|(2[0-3])):([0-5][0-9])$"></asp:RegularExpressionValidator>
                                 <asp:RequiredFieldValidator ID="RequiredFieldValidator3" Display="Dynamic" runat="server" ErrorMessage="Por favor digite um hor�rio." ControlToValidate="TextBoxHorarioSaida"></asp:RequiredFieldValidator>
 <asp:Button ID="ButtonSave" runat="server" Text="Inserir" Width="60px" Height="20px"
                    onclick="ButtonSave_Click" />                                
</td>
        </tr>        
        
 <tr>
            <td>
                </td>
            <td colspan="2">
                <asp:CheckBox ID="CheckBox30Min" runat="server"  Text="Realizou compensa��o de 30 minutos" Checked="true" /></td>
            <td>                                                           
</td>
        </tr>                

    </table>
    <br />

<asp:SqlDataSource ID="SqlDataSourceMotos" runat="server" 
        ConnectionString="<%$ ConnectionStrings:ControledeMotoristasConnectionString %>" 
        SelectCommand="SELECT '' as [Nome], '' as [NumeroUSP] union SELECT [Nome], [NumeroUSP] FROM [Usuarios] where tipo = 'Motorista' and Unidade = @Unidade order by [Nome]">
        <SelectParameters>
                <asp:SessionParameter SessionField="UnidadeUsuarioLogado" Name="Unidade" Type="String" />
        </SelectParameters>
</asp:SqlDataSource>
    <asp:SqlDataSource ID="SqlDataSourceMotoDetails" runat="server" 
        ConnectionString="<%$ ConnectionStrings:ControledeMotoristasConnectionString %>" 
        SelectCommand="SELECT [NumeroUSP], [Nome], [Email], [Telefone], ISNULL((select sum(SaldoMinutos) saldo from FolhaPonto where NumeroUSP = mot.NumeroUSP),0) AS SaldoMinutos  FROM [Usuarios] mot where tipo = 'Motorista' and unidade = @Unidade">       
        <SelectParameters>
                <asp:SessionParameter SessionField="UnidadeUsuarioLogado" Name="Unidade" Type="String" />
        </SelectParameters>        
    </asp:SqlDataSource>
    <br />
<div style ="width:750px; overflow:auto;">
    <asp:GridView ID="GridView1" runat="server" AllowSorting="True" EnableViewState="False"
        AutoGenerateColumns="False" CellPadding="4" DataSourceID="SqlDataSourcePonto" 
        EmptyDataText="There are no data records to display." ForeColor="Black" DataKeyNames="ID"
        GridLines="Vertical" BackColor="White" BorderColor="#DEDFDE" 
        BorderStyle="None" BorderWidth="1px" style="width:1100px">
        <RowStyle BackColor="#F7F7DE" />
        <Columns>
            <asp:TemplateField ShowHeader="False">
                <ItemTemplate>
                    <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="False" 
                        CommandName="Delete" Text="Excluir" OnClientClick="return confirm('Deseja excluir?');"></asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="ID" HeaderText="ID" SortExpression="ID" Visible="false" />
            <asp:BoundField DataField="Entrada" HeaderText="Entrada" ItemStyle-HorizontalAlign="Center"
                SortExpression="Entrada" DataFormatString="{0:dd/MM HH:mm}" >
<ItemStyle HorizontalAlign="Center"></ItemStyle>
            </asp:BoundField>
            <asp:BoundField DataField="Saida" HeaderText="Sa�da" ItemStyle-HorizontalAlign="Center"
                SortExpression="Saida" DataFormatString="{0:dd/MM HH:mm}" >
<ItemStyle HorizontalAlign="Center"></ItemStyle>
            </asp:BoundField>
            <asp:BoundField DataField="InicioIntervalo" 
                HeaderText="In�cio Intervalo"  ItemStyle-HorizontalAlign="Center"
                SortExpression="InicioIntervalo" DataFormatString="{0:HH:mm}" >
<ItemStyle HorizontalAlign="Center"></ItemStyle>
            </asp:BoundField>
            <asp:BoundField DataField="FimIntervalo" HeaderText="Fim Intervalo" ItemStyle-HorizontalAlign="Center"
                SortExpression="FimIntervalo" DataFormatString="{0:HH:mm}" >
<ItemStyle HorizontalAlign="Center"></ItemStyle>
            </asp:BoundField>
            <asp:BoundField DataField="Compensacao" HeaderText="Compensa��o" ItemStyle-HorizontalAlign="Center"
                SortExpression="Compensacao" DataFormatString="{0:HH:mm}" >                
<ItemStyle HorizontalAlign="Center"></ItemStyle>
            </asp:BoundField>
                <asp:TemplateField ItemStyle-HorizontalAlign="Right">
                <HeaderTemplate>
                        Horas Trabalhadas
                </HeaderTemplate>
                    <ItemTemplate>
                    <%# Convert.ToInt32(Eval("QtdMinutosTrabalhados"))/60 %> hr <%# Convert.ToInt32(Eval("QtdMinutosTrabalhados")) % 60 %> Minutos
                    </ItemTemplate>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
                </asp:TemplateField>
               <asp:TemplateField ItemStyle-HorizontalAlign="Right">
                <HeaderTemplate>
                        Saldo do dia
                </HeaderTemplate>
                    <ItemTemplate>
                    
                            <%# Convert.ToInt32(Eval("SaldoMinutos")) / 60 %> hr <%# Convert.ToInt32(Eval("SaldoMinutos")) % 60%> Minutos
                    
                    </ItemTemplate>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
                </asp:TemplateField>                
               
            <asp:BoundField DataField="Ocorrencia" HeaderText="Ocorr�ncia" 
                SortExpression="Ocorrencia" />                
        </Columns>
        <FooterStyle BackColor="#CCCC99" />
        <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" />
        <SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
        <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" />
        <AlternatingRowStyle BackColor="White" />
    </asp:GridView>
    </div>
    <asp:SqlDataSource ID="SqlDataSourcePonto" runat="server" 
        ConnectionString="<%$ ConnectionStrings:ControledeMotoristasConnectionString %>" 
        SelectCommand="SELECT TOP 20 [ID], [Entrada], [Saida], [InicioIntervalo], [FimIntervalo], '&nbsp;&nbsp;'+[Ocorrencia] as Ocorrencia, [Compensacao], [QtdMinutosTrabalhados], [TipoHora], [SaldoMinutos] FROM [FolhaPonto] where [NumeroUSP] = @NumeroUSP order by [Entrada]"
        DeleteCommand="Delete from FolhaPonto where id = @id">
                <SelectParameters>
            <asp:ControlParameter ControlID="DropDownListMotos" Name="NumeroUSP" 
                PropertyName="SelectedValue" Type="String" />
        </SelectParameters>
        <DeleteParameters>
            <asp:ControlParameter ControlID="GridView1" name="id"  PropertyName="SelectedValue" Type="String" />
        </DeleteParameters>
    </asp:SqlDataSource>
    
    <br />
    <asp:SqlDataSource ID="SqlDataSourceInsert" runat="server" 
        ConnectionString="<%$ ConnectionStrings:ControledeMotoristasConnectionString %>" 
        InsertCommand="INSERT INTO FolhaPonto(NumeroUSP, Entrada, Saida, InicioIntervalo, FimIntervalo, Ocorrencia, [Compensacao], [QtdMinutosTrabalhados], [TipoHora], [SaldoMinutos]) VALUES (@NumeroUSP, @Entrada, @Saida, @InicioIntervalo, @FimIntervalo, @Ocorrencia, @Compensacao, @QtdMinutosTrabalhados, @TipoHora, @SaldoMinutos)" 
        SelectCommand="SELECT FolhaPonto.* FROM FolhaPonto">

    </asp:SqlDataSource>
    
</asp:Content>
