﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Text;
using System.Threading;
using System.Web.Profile;
using System.Security.Cryptography;

namespace ControleHorarioMotoristas
{
    public partial class _Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
    
        protected void Login1_Authenticate(object sender, AuthenticateEventArgs e)
        {
            Login login = sender as Login;
            UsuarioTO usuario = UsuarioDAO.get(login.UserName, GetMD5Hash(login.Password));

            Session["UsuarioLogado"] = usuario;
            e.Authenticated = true;// (usuario.ID > 0);

            LogDAO.insert(login.UserName, "Login Success: " + e.Authenticated, Request.UserHostAddress, Request.Browser.Browser + " " + Request.Browser.Version, "Registro de evento");   

        }

        public static string GetMD5Hash(string input)
        {
            // Primeiro passo, calcular o MD5 hash a partir da string
            MD5 md5 = System.Security.Cryptography.MD5.Create();
            byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
            byte[] hash = md5.ComputeHash(inputBytes);

            // Segundo passo, converter o array de bytes em uma string haxadecimal
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < hash.Length; i++)
            {
                sb.Append(hash[i].ToString("X2"));
            }
            return sb.ToString().ToLower();
        }
    }
}
