﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="ControleHorarioMotoristas._Default" validateRequest="false" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Órbita - Controle de O.S.</title>
    <link href="_EstiloCSS/main.css" rel="stylesheet" type="text/css" />
    <link href="_EstiloCSS/controlsDefault.css" rel="stylesheet" type="text/css" />
    </head>
<body>

<form id="form1" runat="server">

    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>


<div class="wrapper">
   
   <!-- Borda do site -->
   <div id="border">
      
      <!-- Cabeçalho login -->
      <div class="header">
          <img id="Img1" src="_Imagens/Corpo/usp_cabecalho.png" style="margin-top:-6px;" runat="server" />
          <br /><br />
      
                <div style="float:left;margin-left:15px">Seja bem-vindo ao portal de sistemas da 
                    Prefeitura do Campus USP &quot;Luiz de Queiroz&quot;</div>

               

	  </div>
      <!-- fim cabeçalho login -->
      <!-- Menu - Barra de navegação -->
	  <!-- fim menu -->
	   <br /><br />
	   
	   
		 <center>
	<!--  <h1>Sistema em manutenção. Breve voltaremos.</h1>  -->
			 
	      <asp:Login ID="Login1" runat="server" BackColor="#EFF3FB"
            BorderColor="#B5C7DE" BorderPadding="4" BorderStyle="Solid" BorderWidth="1px" 
            Font-Names="Verdana" Font-Size="0.8em" ForeColor="#333333" 
            OnAuthenticate="Login1_Authenticate" DestinationPageUrl="~/Formularios/pageCadastroPonto.aspx" 
            FailureText="Senha ou usuário não conferem." LoginButtonText="OK" 
            PasswordLabelText="Senha:" PasswordRequiredErrorMessage="Senha requerida." 
            RememberMeText="Deixar gravado usuário e senha." 
            TitleText="Acessar o sistema Órbita" UserNameLabelText="Nº USP:" 
            UserNameRequiredErrorMessage="Usuário Requerido." DisplayRememberMe="False" 
              InstructionText="Acesse com seu número USP" Height="91px" Width="199px" Orientation="Horizontal" TextLayout="TextOnTop">
            <TextBoxStyle Font-Size="0.8em" />
            <LoginButtonStyle BackColor="White" BorderColor="#507CD1" BorderStyle="Solid" 
                BorderWidth="1px" Font-Names="Verdana" Font-Size="0.8em" ForeColor="#284E98" />
              <LayoutTemplate>
                  <table border="0" cellpadding="4" cellspacing="0" 
                      style="border-collapse:collapse;">
                      <tr>
                          <td>
                              <table border="0" cellpadding="0" style="height:91px;width:199px;">
                                  <tr>
                                      <td align="center" colspan="2" 
                                          style="color:White;background-color:#507CD1;font-weight:bold;">
                                          Acessar o sistema Órbita</td>
                                  </tr>
                                  <tr>
                                      <td align="center" colspan="2" style="color:Black;font-style:italic;">
                                          Acesse com seu número USP</td>
                                  </tr>
                                  <tr>
                                      <td align="right">
                                          <asp:Label ID="UserNameLabel" runat="server" AssociatedControlID="UserName">Nº USP:</asp:Label>
                                      </td>
                                      <td>
                                          <asp:TextBox ID="UserName" runat="server" Height="21px" 
                                              Width="106px"></asp:TextBox>
                                          <asp:RequiredFieldValidator ID="UserNameRequired" runat="server" 
                                              ControlToValidate="UserName" ErrorMessage="Usuário Requerido." 
                                              ToolTip="Usuário Requerido." ValidationGroup="Login1">*</asp:RequiredFieldValidator>
                                      </td>
                                  </tr>
                                  <tr>
                                      <td align="right">
                                          <asp:Label ID="PasswordLabel" runat="server" AssociatedControlID="Password">Senha:</asp:Label>
                                      </td>
                                      <td>
                                          <asp:TextBox ID="Password" runat="server" Height="21px" 
                                              TextMode="Password" Width="106px"></asp:TextBox>
                                          <asp:RequiredFieldValidator ID="PasswordRequired" runat="server" 
                                              ControlToValidate="Password" ErrorMessage="Senha requerida." 
                                              ToolTip="Senha requerida." ValidationGroup="Login1">*</asp:RequiredFieldValidator>
                                      </td>
                                  </tr>
                                  <tr>
                                      <td align="center" colspan="2" style="color:Red;">
                                          <asp:Literal ID="FailureText" runat="server" EnableViewState="False"></asp:Literal>
                                      </td>
                                  </tr>
                                  <tr>
                                      <td align="right" colspan="2">
                                          <asp:Button ID="LoginButton" runat="server" BackColor="White" 
                                              BorderColor="#507CD1" BorderStyle="Solid" BorderWidth="1px" CommandName="Login" 
                                              Font-Names="Verdana" Font-Size="0.8em" ForeColor="#284E98" Height="27px" 
                                              Text="OK" ValidationGroup="Login1" Width="79px" />
                                      </td>
                                  </tr>
                              </table>
                          </td>
                      </tr>
                  </table>
              </LayoutTemplate>
            <InstructionTextStyle Font-Italic="True" ForeColor="Black" />
            <TitleTextStyle BackColor="#507CD1" Font-Bold="True" Font-Size="0.9em" 
                ForeColor="White" />
        </asp:Login>
        <!--
             <a href="Usuario/pageUsuarioEsqueciSenha.aspx">
             esqueci minha senha</a>
         | 
             <a href="Usuario/pageUsuarioCadastro.aspx" visible="false"> novo usuário
             </a>
        -->
         </center>
         
         
		    <br />
&nbsp;&nbsp;&nbsp;&nbsp;<br />
       <br />
       &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
       Spark&nbsp;&nbsp;&nbsp; |&nbsp;&nbsp;&nbsp; TeamViewer&nbsp;&nbsp;&nbsp; |&nbsp;&nbsp;&nbsp; 
       <br />
&nbsp;<!-- Conteúdo Principal a direita --><!-- End Sponsor Ads Box --><!-- Start Main Content -->
		  <!-- End Main Content -->		  	  
		  <div class="maincontent" style="width:778px">
			 <div style="width:778px">
			   <asp:Image ID="Image3" width="778px" runat="server" CssClass="block" DescriptionUrl="" ImageUrl="_Imagens/Corpo/content-top.gif" />
			 </div>
			 
			 <div class="content-mid-full">
<h1>Últimas mensagens:</h1>
                 <p>&nbsp;</p>
                 
        	 </div>
			 
			 <div style="width:778px">
			   <asp:Image ID="Image4"  width="778px"  runat="server" CssClass="block" DescriptionUrl="" ImageUrl="_Imagens/Corpo/content-bottom.gif" />		       
			 </div>
		  </div>		  
     
	  <!-- End Right Column -->
	
       <br />
	
   </div>
   <!-- End Border -->
   
   <!-- Start Footer -->
	  <div id="footer">
	     &copy; Copyright 2010   </div>
   <!-- End Footer -->
   
</div> 
</form>   

   
</body>
</html>
